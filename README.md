# 1001 Programs in BASIC

Common library of BASIC routines, runs on [Chipmunk BASIC](http://www.nicholson.com/rhn/basic/).

Would be nice if it was more xplat, but one good, functionally usable BASIC is better. Automated translation may be practical.

Example programs are often copied^W inspired by '80s BASIC books and magazines, but all code & text is my own.

### Screenshots

- <a href='screenshots/002-seedice.png'><img src='screenshots/002-seedice.png' width='320' height='240'></a>

### License

Everything's under the BSD License. An ye harm none, do what thou will shall be the whole of the law.
