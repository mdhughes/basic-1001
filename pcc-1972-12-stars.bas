#!/usr/bin/env basic
# from cover of PCC newsletter 1972-12

1 merge "common.bas"
2 commoninit()

10 for y=1 to ym
15	for x=1 to xm
20		if rnd(1)<0.1 then ?"*"; else ?" ";
25	next x
30	?
31	pause(250)
35 next y
40 upkey$()

999 endprog(0)
