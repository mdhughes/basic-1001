# Common routines for BASIC programming
# by Mark Damon Hughes, 2021
# BSD License
#
# Uses Chipmunk BASIC http://www.nicholson.com/rhn/basic/
#
# Structure of any program using this library:
# ---
#	#!/usr/bin/env basic
#	1 merge "common.bas"
#	2 commoninit()
#	10-199: setup
#	1000-1999: update
#	2000-2999: redraw
#	3000-3999: events/input
#	5000-9999: user functions & init
#	10000-19999: user data
#	At end, call `endprog(0)`
# ---
# Line numbers are optional, but may be useful.
# Library functions start at line 20000, no user code should be above 19999
#
# Assumes 80x25 screen (xm,ym variables)
# Uses VT100 positioning, colors, & limited Unicode chars.
# When exporting to older BASICs, remove all ^# comment lines.

# *** System Functions ***

# endprog(n)
# End program, exit with status `n` (0 for normal)
20000 sub endprog(n)
	color("0")
	exit n
end sub

# rolldice(ndice, sdice) -> total
# Rolls `ndice` (1-30) dice of `sdice` (2-1000) sides, fills in array `dice()`
sub rolldice(ndice, sdice)
	local i0, tdice
	if ndice<1 or ndice>30 or sdice<2 or sdice>1000 then ?"Bad dice ";ndice;"d";sdice:endprog(1)
	tdice=0
	for i0=1 to ndice
		dice(i0)=int(rnd(1)*sdice)+1:tdice=tdice+dice(i0)
	next i0
	for i0=ndice+1 to 30:dice(i0)=0:next i0
	rolldice=tdice
end sub

# pause(ms) -> 0
# Pauses the program for `ms` milliseconds.
sub pause(ms)
	fn sleep(ms/1000)
end sub

# *** Console Functions ***

# clears() -> 0
# Clear screen & reset colors.
sub clears()
	color("0")
	?esc$;"[0m";esc$;"[H";esc$;"[2J";
end sub

# position(gx, gy) -> 0
# moves cursor to `gx`,`gy`
sub position(gx, gy)
	if gx<1 or gx>xm or gy<1 or gy>ym then ?"Bad cursor ";gx;",";gy:endprog(1)
	?esc$;"[";str$(gy);";";str$(gx);"H";
end sub

# color(color$) -> 0
# Sets ANSI colors to color$, semicolon-delimited color numbers
#	0	Reset all attributes
#	1	Bright
#	2	Dim
#	4	Underscore
#	5	Blink
#	7	Reverse
#	8	Hidden
#	Foreground Color		Background Color
#	30	Black			40	Black
#	31	Red			41	Red
#	32	Green			42	Green
#	33	Yellow			43	Yellow
#	34	Blue			44	Blue
#	35	Magenta			45	Magenta
#	36	Cyan			46	Cyan
#	37	White			47	White
#
sub color(color$)
	? esc$;"[";color$;"m";
end sub

# upkey$() -> upkey$
# Waits for a keypress, uppercases it. What up with you?
sub upkey$()
	local u0$
	while
		u0$=ucase$(inkey$)
	wend u0$<>""
	upkey$=u0$
end sub

# yesno() -> yn
# Prompts, reads "Y" or "N", prints response, returns 0 or 1
sub yesno()
	local u0$, yn0
	?" (Y/N)? ";
	yn0=-1
	while
		u0$=upkey$()
		if u0$="Y" then ?"Yes":yn0=1
		if u0$="N" then ?"No":yn0=0
	wend yn0>-1
	yesno=yn0
end sub

# *** Graphics Functions ***

# dicedraw(x, y, ddice) -> 0
# Draws a 6-sided die `ddice` with pips, as 5x5 character grid
sub dicedraw(x, y, ddice)
	local x2, y2, i0
	boxdraw(x, y, x+4, y+4)
	for i0=1 to 3
		position(x+1, y+i0):?dicepip$(i0, ddice);
	next i0
end sub

# boxdraw(x1, y1, x2, y2) -> 0
# Draws a box with characters.
sub boxdraw(x1, y1, x2, y2)
	local x0, y0
	position(x1, y1)
	?box$(1,1);:for x0=x1+1 to x2-1:?box$(4,1);:next x0:?box$(3,1);
	for y0=y+1 to y2-1
		position(x1, y0)
		?box$(4,2);:for x0=x1+1 to x2-1:?" ";:next x0:?box$(4,2);
	next y0
	position(x1, y2)
	?box$(1,3);:for x0=x1+1 to x2-1:?box$(4,1);:next x0:?box$(3,3);
end sub

# commoninit()
# Sets up common data
25000 sub commoninit()
	local r0, x0, y0
	randomize timer():r0=rnd(1)
	rem ASCII controls
	bel$=chr$(0x07):bs$=chr$(0x08):nl$=chr$(0x0A):esc$=chr$(0x1B)
	rem screen size
	xm=80:ym=25 :rem PLAT
	dim box$(4,3)
	restore 25000:for y0=1 to 3:for x0=1 to 4:read a$:box$(x0,y0)=a$:next x0:next y0
	dim dicepip$(3,6)
	restore 25010:for y0=1 to 6:for x0=1 to 3:read a$:dicepip$(x0,y0)=a$:next x0:next y0
	dim dice(30)
	rem TODO: more init
end sub

25500 rem Box characters :rem PLAT
# Unicode
25501 data "┏", "┳", "┓", "━"
25502 data "┣", "╋", "┫", "┃"
25503 data "┗", "┻", "┛", "⬛"
# ASCII-only
#25501 data "/", "+", "\", "-"
#25502 data "+", "+", "+", "|"
#25503 data "\", "+", "/", "#"

25510 rem Dice pips :rem PLAT
# Unicode
25511 data "   ", " • ", "   "
25512 data "•  ", "   ", "  •"
25513 data "  •", " • ", "•  "
25514 data "• •", "   ", "• •"
25515 data "• •", " • ", "• •"
25516 data "• •", "• •", "• •"
# ASCII-only
#25511 data "   ", " * ", "   "
#25512 data "*  ", "   ", "  *"
#25513 data "  *", " * ", "*  "
#25514 data "* *", "   ", "* *"
#25515 data "* *", " * ", "* *"
#25516 data "* *", "* *", "* *"
