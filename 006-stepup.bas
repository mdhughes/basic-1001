#!/usr/bin/env basic
1 merge "common.bas"
2 commoninit()
5 gosub 5000

100 num$=num$+str$( int(rnd(1)*10) )

200 clears():?"Memorize this number:"
205 position(8,3):?num$
210 pause(500):gosub 1100
215 clears():position(1,5):?"Round ";len(num$)

300 input "What was that number? ",g$
305 if g$=num$ then ?"YES! You advance to the next round!":goto 100
310 ?"NO!!! It was ";num$:?"You could only remember ";str$(len(num$)-1);" digits."
315 ?"Try again";:if yesno() then goto 5

999 endprog(0)

5000 num$=""
5099 return
