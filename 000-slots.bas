#!/usr/bin/env basic
1 merge "common.bas"
2 commoninit()
5 gosub 5000
10 goto 300

100 for y=1 to 3:for x=1 to 3
105	j=int(rnd(1)*16)+1
110	w(x,y)=j
115 next x:next y

200 for i=1 to 3
205	?box$(1,1);box$(4,1);box$(4,1);box$(4,1);box$(3,1);" ";
210 next i
215 ?

220 for y=1 to 3
225	for x=1 to 3
230		?box$(4,2);p$(w(x,y));box$(4,2);" ";
235	next x
240 ?:next y

245 for i=1 to 3
250	?box$(1,3);box$(4,1);box$(4,1);box$(4,1);box$(3,3);" ";
255 next i
260 ?

270 s=0:mt=0
271 if w(1,1)=w(2,1) and w(2,1)=w(3,1) then ?"*** Top ";:s=s+3
272 if w(1,2)=w(2,2) and w(2,2)=w(3,2) then ?"*** Middle ";:mt=1:s=s+9
273 if w(1,3)=w(2,3) and w(2,3)=w(3,3) then ?"*** Bottom ";:s=s+3
274 if w(1,1)=w(1,2) and w(1,2)=w(1,3) then ?"*** Left ";:s=s+3
275 if w(2,1)=w(2,2) and w(2,2)=w(2,3) then ?"*** Center ";:s=s+3
276 if w(3,1)=w(3,2) and w(3,2)=w(3,3) then ?"*** Right ";:s=s+3
277 if not mt and (w(1,2)=w(2,2) or w(2,2)=w(3,2)) then ?"*** Double ";:s=s+2

280 if s=0 then ?"Nothing, try again!":goto 300
285 ?"***":?"Pays out $";s;"!"
290 for i=1 to s:?bel$;:pause(250):next i
295 score=score+s:house=house-s

300 ?:?
305 ?"You have $";score
310 if score=0 then goto 400
315 ?"Pull the lever";
320 if yesno() then goto 450
325 ?"You walk away with $";score
330 ?"The house made $";house
335 endprog(0)

400 ?"BUSTED OUT"
405 ?"The house made $";house
410 endprog(0)

450 clears():?bel$;"You put in $1":pause(250)
455 score=score-1:house=house+1
460 goto 100

5000 clears():?"One-Armed Bandit"
5005 ?"  Instructions: $1 per pull."
5010 ?"  Each triple pays 3x, middle row pays 9x!"
5015 ?"  Middle row double pays 2x!"
5020 dim w(3,3),p$(16):score=20:house=0
5025 restore 11000:for i=1 to 16:read a$:p$(i)=a$:next i
5099 return

11000 data "-0-","-1-","-2-","-3-","-4-","-5-","-6-","-7-"
11001 data "-8-","-9-","-A-","-B-","-C-","-D-","-E-","-F-"
