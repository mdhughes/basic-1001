#!/bin/zsh
if [[ $# -ne 2 ]]; then
	echo "Usage: mkbas.zsh NUM NAME"
	exit 1
fi

f=`printf "%03d-%s.bas" $1 $2`

cat <<HERE >$f
#!/usr/bin/env basic
1 merge "common.bas"
2 gosub 10000
10 clears():?"$2"
999 endprog(0)

HERE

chmod 755 $f
