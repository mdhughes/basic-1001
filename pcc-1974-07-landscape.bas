#!/usr/bin/env basic
# by Gregory Yob, PCC newsletter 1974-07

1 merge "common.bas"
2 commoninit()

10 x=1:y=1

100 z=rnd(1)
105 if z<0.5 then ?"...";:goto 150
110 if z>=0.9 then ?"|_|";:goto 150
115 if z>=0.8 then ?"/^\";:goto 150
120 if z>=0.7 then ?"*@*";:goto 150
125 ?"   ";

150 x=x+3
155 if x<=xm-3 then goto 100
160 ?
165 x=1:y=y+1
170 if y>=ym then goto 200
175 pause(250)
180 goto 100

200 upkey$()

999 endprog(0)
