#!/usr/bin/env basic
1 merge "common.bas"
2 commoninit()
5 gosub 5000

100 roll=roll+1
105 tdice=rolldice(ndice,sdice)

200 clears()
205 ?"Roll #";str$(roll)
210 for i=1 to ndice
215	dicedraw((i-1)*6+2, 3, dice(i))
220 next i

300 position(1,9):?"Rolled ";
305 for i=1 to ndice:?str$(dice(i));", ";:next i
310 ?"total ";tdice
315 ?:?"Roll again";:if yesno() then goto 100

999 endprog(0)

5000 input "Roll how many dice (1-10)? ",ndice
5005 if ndice<1 or ndice>10 or ndice<>int(ndice) then ?"Nope.":goto 5000
5010 roll=0:sdice=6
5099 return
