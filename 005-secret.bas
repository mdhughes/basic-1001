#!/usr/bin/env basic
1 merge "common.bas"
2 commoninit()
5 gosub 5000

200 ?"Plain: ";
205 for i=1 to 26
210	?chr$(64+i);" ";
215 next i
220 ?:?"Code:  ";
225 for i=1 to 26
230	?chr$(64+c(i));" ";
235 next i
240 ?

999 endprog(0)

5000 clears():?"Secret Code"
5005 dim a(26),c(26)
5010 for i=1 to 26:a(i)=i:next i
5015 for i=1 to 26 :rem overly slow shuffle
5020	j=int(rnd(1)*26)+1:if a(j)=0 then goto 5020
5025	c(i)=a(j):a(j)=0
5030 next i
5099 return
