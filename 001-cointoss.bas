#!/usr/bin/env basic
1 merge "common.bas"
2 commoninit()
5 gosub 5000

100 c=c+1
105 flip=int(rnd(1)*2)

200 clears():?"Flipping coin...":?:?
205 ?"Coin #";c;": ";
210 if flip=0 then ?"Heads" else ?"Tails"

300 ?:?
305 ?"Flip again";:if yesno() then goto 100

999 endprog(0)

5000 c=0
5099 return
