#!/usr/bin/env basic
1 merge "common.bas"
2 commoninit()
5 clears():?"Countdown"
10 ?"Press L to launch, A to abort: ";
15 u$=upkey$()
16 if u$="L" then ?"Launch!":goto 50
17 if u$="A" then ?"Abort!":endprog(0)
18 goto 15

50 ?"Counting down..."
51 for i=10 to 1 step -1
52	if i>3 then color("1;37") else color("1;31")
54	for j=1 to i:?"   ";:next j
55	?bel$;i
56	pause(1000)
57 next i

60 ?"Blastoff!"
61 for i=1 to 10
62	?bel$;
63	pause(200)
64 next i

999 endprog(0)
